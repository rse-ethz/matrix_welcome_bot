import re
import signal
import subprocess
import sys
import threading

from fastapi import FastAPI

app = FastAPI()


class StartBot(threading.Thread):
    def __init__(self):
        self.session_id = None
        self.fingerprint = None
        self.p = None
        super().__init__()

    def run(self):
        cmd = [sys.executable, "-u", "bot.py"]

        self.p = p = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            text=True,
            close_fds=True,
        )

        for line in iter(p.stdout.readline, ""):
            print(">", line.rstrip())
            match = re.match(r"Connected to .* \(([A-Z]+)\)", line)

            if match:
                self.session_id = match.groups()[0]

            match = re.match(r"This bot's public fingerprint .* is: (.*)", line)
            if match:
                self.fingerprint = match.groups()[0]

    def stop(self, *_):
        if self.p:
            print("terminate")
            self.p.terminate()


t = StartBot()
t.start()


def sig_handler(signal, frame):
    print("received signal", signal)
    t.stop()
    t.join()
    sys.exit()


@app.on_event("startup")
async def startup_event():
    signal.signal(signal.SIGINT, sig_handler)


@app.get("/")
def index():
    if t.session_id is None or t.fingerprint is None:
        return dict(
            ok=False,
            message="bot not started yet",
        )
    return dict(
        ok=True,
        message="bot started",
    )


@app.get("/auth_data")
def auth_data():
    if t.session_id is None or t.fingerprint is None:
        return dict(
            ok=False,
        )
    return dict(
        ok=True,
        session_id=t.session_id,
        fingerprint=t.fingerprint,
    )
