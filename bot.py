import functools
import os


from aiohttp.client import ClientSession

orig_get = ClientSession.get


def get(self, url):
    proxy = os.environ.get("HTTPS_PROXY")
    return orig_get(self, url, proxy=proxy)


ClientSession.get = get


from nio import AsyncClient

orig_init = AsyncClient.__init__


def init(*a, **kw):
    kw["proxy"] = os.environ.get("HTTPS_PROXY")
    return orig_init(*a, **kw)


AsyncClient.__init__ = init


import simplematrixbotlib as botlib
import yaml
from nio.events.room_events import RoomMemberEvent

MATRIX_SERVER = os.environ.get("MATRIX_SERVER", "https://matrix.org")
MATRIX_ACCOUNT = os.environ.get("MATRIX_ACCOUNT")
MATRIX_PASSWORD = os.environ.get("MATRIX_PASSWORD")

STORE_PATH = os.environ.get("STORE_PATH", "/store")
MESSAGES_YAML = os.environ.get("MESSAGES_YAML", "/messages.yaml")


print = functools.partial(print, flush=True)
print(print)


os.makedirs(STORE_PATH, exist_ok=True)


creds = botlib.Creds(
    MATRIX_SERVER,
    MATRIX_ACCOUNT,
    MATRIX_PASSWORD,
    session_stored_file=os.path.join(STORE_PATH, "session.txt"),
)

config = botlib.Config()
config.encryption_enabled = True
config.emoji_verify = True
config.ignore_unverified_devices = True
config.store_path = STORE_PATH
config.emoji_verify = False


bot = botlib.Bot(creds, config)


def get_message(room_id):
    with open(MESSAGES_YAML, "r") as fh:
        messages = yaml.load(fh, yaml.Loader)
    return messages.get(room_id)


@bot.listener.on_custom_event(RoomMemberEvent)
async def room_joined(room, event):
    if event.membership != "join":
        return

    message = get_message(room.room_id)
    if message is None:
        return

    message = message.format(displayname=event.source["content"]["displayname"])
    await bot.api.send_markdown_message(
        room.room_id,
        message,
        msgtype="m.notice",
    )


print("run")
bot.run()
