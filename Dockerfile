FROM python:3.12-slim-bookworm
RUN apt-get update && apt-get install -y vim curl
COPY requirements.txt /
RUN  pip install --user -r requirements.txt
COPY bot.py start_bot.py /
ENTRYPOINT ["/root/.local/bin/uvicorn", "--host", "0.0.0.0", "--port", "8000", "start_bot:app"]

